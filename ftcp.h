#pragma once
#include <stdint.h>
#include <stdlib.h>
#include <libiptc/libiptc.h>
struct faketcp {
	int rcvfd;
	int sndfd;
	uint32_t src;
	uint16_t rcv_port;
};

struct ftcp_opt {
	uint8_t kind;
	uint8_t len;
	uint8_t val[];
};

struct ftcp_hdr {
	uint32_t addr;
	uint32_t seq;
	uint32_t ack;
	uint16_t sport;
	uint16_t dport;
	uint8_t flags;
	uint8_t optlen;
	uint8_t optbuf[40];

	uint8_t nopt;
	struct ftcp_opt *opt[40];
};
// port in local order
int init_ftcp(struct faketcp *, uint32_t saddr, uint16_t port);
int close_ftcp(struct faketcp *);
ssize_t ftcp_snd(struct faketcp *s, const uint8_t *data, size_t len,
                 const struct ftcp_hdr *hdr);
ssize_t ftcp_rcv(struct faketcp *s, const char **data, struct ftcp_hdr *hdr);

static inline void dump_hdr(const struct ftcp_hdr *hdr) {
	printf("addr: %s\n", inet_ntoa((struct in_addr){hdr->addr}));
	printf("dp: %d sp: %d\n", ntohs(hdr->dport), ntohs(hdr->sport));
	if (hdr->flags&2)
		printf("SYN ");
	if (hdr->flags&16)
		printf("ACK ");
	if (hdr->flags&4)
		printf("RST ");
	printf("\n");
}
