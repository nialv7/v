#include <arpa/inet.h>
#include <assert.h>
#include <endian.h>
#include <ev.h>
#include <netinet/ip.h>
#include <poll.h>
#include <sodium/crypto_aead_xchacha20poly1305.h>
#include <sodium/crypto_generichash.h>
#include <sodium/crypto_kx.h>
#include <sodium/randombytes.h>
#include <sodium/utils.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "ftcp.h"
#include "tun.h"
#include "vcom.h"

static struct connection *current = NULL;
static struct faketcp s;
static struct peer server;
static uint8_t sk[2][crypto_kx_SECRETKEYBYTES];
static uint8_t pk[2][crypto_kx_PUBLICKEYBYTES];
static uint8_t challenge[32];
static ev_io tunw, sockw;
static ev_timer syn_retry_timer, pulse_timer;
static const char *passwd;
static uint32_t saddr;

_Static_assert(crypto_pwhash_SALTBYTES <= crypto_kx_SESSIONKEYBYTES, "kx session key is too "
                                                                     "small");

static void sock_cb(EV_P_ ev_io *w, int revents);
static void syn_retry(EV_P_ ev_timer *t, int revents) {
	fprintf(stderr, "send SYN\n");
	uint8_t buf[IP_MAXPACKET];
	for (int i = 0; i < 2; i++)
		crypto_kx_keypair(pk[i], sk[i]);
	memcpy(buf, pk[0], crypto_kx_PUBLICKEYBYTES);
	memcpy(buf + crypto_kx_PUBLICKEYBYTES, pk[1], crypto_kx_PUBLICKEYBYTES);
	randombytes_buf(challenge, 32);
	memcpy(buf + 2 * crypto_kx_PUBLICKEYBYTES, challenge, 32);

	struct ftcp_hdr hdr;
	hdr.sport = htons(s.rcv_port);
	hdr.dport = server.sport;
	hdr.addr = server.src;
	hdr.flags = 2;        // SYN;
	hdr.ack = 0;
	hdr.nopt = 0;
	hdr.seq = randombytes_random();

	ftcp_snd(&s, buf, 2 * crypto_kx_PUBLICKEYBYTES + 32, &hdr);
}
static inline void client_initiate(void) {
	if (s.sndfd > 0 || s.rcvfd > 0) {
		ev_io_stop(EV_DEFAULT, &sockw);
		close_ftcp(&s);
	}

	uint16_t port = randombytes_random();
	ssize_t ret = init_ftcp(&s, saddr, port);
	assert(ret >= 0);
	ev_io_set(&sockw, s.rcvfd, EV_READ);
	ev_io_start(EV_DEFAULT, &sockw);

	ev_timer_set(&syn_retry_timer, 0, 1.5);
	ev_timer_start(EV_DEFAULT, &syn_retry_timer);
}

static inline void server_auth(struct faketcp *s, const struct ftcp_hdr *hdr,
                               const uint8_t *data, size_t dlen, const char *passwd) {
	if (!peer_match(&server, hdr)) {
		rst(s, hdr);
		return;
	}

	if (dlen != 2 * crypto_kx_PUBLICKEYBYTES + crypto_onetimeauth_BYTES + 32) {
		fprintf(stderr, "malformed server reply\n");
		dump_hdr(hdr);
		return;
	}
	const uint8_t *spk[2];
	spk[0] = (const uint8_t *)data;
	spk[1] = (const uint8_t *)data + crypto_kx_PUBLICKEYBYTES;

	struct connection *c = malloc(sizeof *c);
	int ret2 = crypto_kx_client_session_keys(c->rcv_key, c->snd_key, pk[0], sk[0], spk[0]);
	sodium_memzero(sk[0], crypto_kx_SECRETKEYBYTES);
	if (ret2 != 0) {
		free(c);
		return;
	}

	uint8_t tmp[2][crypto_kx_SESSIONKEYBYTES];
	ret2 = crypto_kx_client_session_keys(tmp[0], tmp[1], pk[1], sk[1], spk[1]);
	sodium_memzero(sk[1], crypto_kx_SECRETKEYBYTES);
	if (ret2 != 0) {
		free(c);
		return;
	}

	const uint8_t *auth = (const uint8_t *)data + 2 * crypto_kx_PUBLICKEYBYTES;
	if (verify_challenge(challenge, 32, tmp[1], passwd, auth) != 0) {
		fprintf(stderr, "failed to verify server response\n");
		return;
	}

	fprintf(stderr, "server SYN/ACK OK.\n");
	ev_timer_stop(EV_DEFAULT, &syn_retry_timer);
	ev_timer_again(EV_DEFAULT, &pulse_timer);
	ev_io_start(EV_DEFAULT, &tunw);
	c->p.src = hdr->addr;
	c->p.sport = hdr->sport;
	c->tseq = 0;
	c->rseq = 0;
	c->cookie = NULL;

	current = c;

	// send ACK
	const uint8_t *sch =
	    (const uint8_t *)data + 2 * crypto_kx_PUBLICKEYBYTES + crypto_onetimeauth_BYTES;
	uint8_t cauth[crypto_onetimeauth_BYTES];
	compute_challenge(sch, 32, tmp[0], passwd, cauth);

	struct ftcp_hdr nhdr;
	nhdr.sport = htons(s->rcv_port);
	nhdr.dport = server.sport;
	nhdr.addr = server.src;
	nhdr.flags = 16;        // ACK;

	nhdr.ack = hdr->seq + dlen + 1;
	nhdr.seq = hdr->ack;
	nhdr.nopt = 0;
	ftcp_snd(s, cauth, sizeof cauth, &nhdr);
}

static ev_timer reconnect_timer;
static inline void syn_with_cookie(struct faketcp *s, struct ftcp_hdr *hdr) {
	fprintf(stderr, "send fast open SYN\n");
	struct ftcp_hdr nhdr;
	nhdr.sport = htons(s->rcv_port);
	nhdr.dport = current->p.sport;
	nhdr.addr = current->p.src;
	nhdr.flags = 2;        // SYN

	nhdr.ack = 0;
	nhdr.seq = randombytes_random();
	current->seq = nhdr.seq + 1;

	nhdr.nopt = 1;
	char buf[20];
	struct ftcp_opt *opt = (void *)buf;
	opt->kind = 34;
	opt->len = 18;
	memcpy(opt->val, current->cookie, 16);
	nhdr.opt[0] = opt;
	ftcp_snd(s, NULL, 0, &nhdr);
}
static inline void
reestablish_connection(struct faketcp *s, struct ftcp_hdr *hdr, const uint8_t *data, size_t dlen) {
	static uint8_t buf[128];
	unsigned long long mlen = sizeof buf;
	uint8_t nonce[24];
	memcpy(nonce, data, 24);
	int ret = crypto_aead_xchacha20poly1305_ietf_decrypt(buf, &mlen, NULL, data + 24, dlen - 24,
	                                                     NULL, 0, nonce, current->rcv_key);

	ev_io_stop(EV_DEFAULT, &sockw);
	close_ftcp(s);
	uint16_t port = randombytes_random();

	init_ftcp(s, s->src, port);
	ev_io_set(&sockw, s->rcvfd, EV_READ);
	ev_io_start(EV_DEFAULT, &sockw);
	if (ret != 0 || mlen != 16) {
		fprintf(stderr, "server cookie invalid, reconnect\n");
		client_initiate();
		return;
	}
	fprintf(stderr, "got server cookie\n");
	current->cookie = buf;
	syn_with_cookie(s, hdr);
	// hold tun incoming
	ev_io_stop(EV_DEFAULT, &tunw);
	ev_timer_again(EV_DEFAULT, &reconnect_timer);
}
static inline void reestablish_connection_done(struct faketcp *s, struct ftcp_hdr *hdr) {
	struct ftcp_hdr nhdr = *hdr;
	nhdr.dport = hdr->sport;
	nhdr.sport = htons(s->rcv_port);
	nhdr.flags = 16;        // ACK
	nhdr.ack = hdr->seq + 1;
	nhdr.seq = current->seq++;
	nhdr.nopt = 0;

	current->ack = hdr->seq;
	current->cookie = NULL;
	ev_io_start(EV_DEFAULT, &tunw);
	ev_timer_stop(EV_DEFAULT, &reconnect_timer);
	ev_timer_again(EV_DEFAULT, &pulse_timer);
	fprintf(stderr, "connection reestablished\n");
	ftcp_snd(s, NULL, 0, &nhdr);
}

static void reconnect_timeout(EV_P_ ev_timer *t, int revents) {
	struct ftcp_hdr hdr;
	hdr.addr = current->p.src;
	hdr.sport = current->p.sport;
	syn_with_cookie(&s, &hdr);
}

static size_t queued;
static uint8_t data_queue[IP_MAXPACKET];
static void tun_timeout_cb(EV_P_ ev_timer *t, int revents) {
	ev_timer_stop(EV_A_ & tun_timer);
	if (!current || current->cookie)
		return;
	if (queued < PWATERMARK) {
		queued += addpad(data_queue+queued, PWATERMARK-queued);
	}
	buf2remote(data_queue, queued, s.rcv_port, &s, current);
	queued = 0;
}
static void tun_cb(EV_P_ ev_io *w, int revents) {
	if (!current || current->cookie)
		return;
	tun2buf(w->fd, data_queue, &queued, s.rcv_port, &s, current);
}

static void server_timeout(EV_P_ ev_timer *t, int revents) {
	ev_timer_stop(EV_DEFAULT, &pulse_timer);
	ev_timer_stop(EV_DEFAULT, &reconnect_timer);
	ev_io_stop(EV_DEFAULT, &tunw);

	struct ftcp_hdr hdr;
	hdr.addr = current->p.src;
	hdr.sport = current->p.sport;
	hdr.seq = current->ack;
	hdr.ack = current->seq;
	rst(&s, &hdr);

	fprintf(stderr, "reconnect to server\n");
	free_connection(current);
	current = NULL;
	client_initiate();
}

static void sock_cb(EV_P_ ev_io *w, int revents) {
	const char *rep = NULL;
	struct ftcp_hdr hdr;
	ssize_t ret = ftcp_rcv(&s, &rep, &hdr);
	if (ret < 0)
		return;
	if (hdr.flags == 16) {
		ev_timer_again(EV_DEFAULT, &pulse_timer);
		remote2tun(tunw.fd, &hdr, cu8(rep), ret, current);
	} else if (hdr.flags == (4 | 16)) {
		fprintf(stderr, "server RST\n");
		if (ret == 0) {
			fprintf(stderr, "server closed\n");
			if (current)
				server_timeout(NULL, NULL, 0);
			return;
		}
		ev_timer_again(EV_DEFAULT, &pulse_timer);
		reestablish_connection(&s, &hdr, cu8(rep), ret);
	} else if (hdr.flags == (16 | 2)) {
		if (!current)
			server_auth(&s, &hdr, cu8(rep), ret, passwd);
		else if (current->cookie)
			reestablish_connection_done(&s, &hdr);
		else
			fprintf(stderr, "server sent syn/ack??\n");
	}
}

static void sig_cb(EV_P_ ev_signal *s, int revents) {
	ev_break(EV_DEFAULT, EVBREAK_ALL);
}

// XXX: consider padding the handshakes
int main(int argc, const char **argv) {
	if (argc < 5)
		return -1;
	ev_timer_init(&syn_retry_timer, syn_retry, 1.5, 1);
	ev_init(&tun_timer, tun_timeout_cb);
	tun_timer.repeat = 0.05;
	ev_init(&reconnect_timer, reconnect_timeout);
	reconnect_timer.repeat = 0.5;
	ev_init(&pulse_timer, server_timeout);
	pulse_timer.repeat = 5;

	inet_aton(argv[1], (void *)&saddr);

	s.sndfd = s.rcvfd = -1;

	inet_aton(argv[2], (void *)&server.src);
	server.sport = htons(atoi(argv[3]));

	int tfd = create_tuntap(argv[4], 1);

	passwd = argv[5];

	ev_io_init(&tunw, tun_cb, tfd, EV_READ);
	ev_init(&sockw, sock_cb);

	ev_signal intsig, termsig;
	ev_signal_init(&intsig, sig_cb, SIGINT);
	ev_signal_init(&termsig, sig_cb, SIGTERM);
	ev_signal_start(EV_DEFAULT, &intsig);
	ev_signal_start(EV_DEFAULT, &termsig);

	client_initiate();
	ev_run(EV_DEFAULT, 0);

	close_ftcp(&s);
	return 0;
}
