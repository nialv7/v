#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <linux/filter.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <netinet/ip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
// to use faketcp, you must use iptables to DROP tcp packets send to
// the port you are using
#include "ftcp.h"
#define auto __auto_type
struct sock_filter f[] = {
    {0x30, 0, 0, 0x00000009},        // ip.proto
    {0x15, 0, 3, 0x00000006},        // tcp
    {0x28, 0, 0, 0x00000016},        // tcp.dst_port
    {0x15, 0, 1, 0x0000226b},        // 8811
    {0x06, 0, 0, 0xffffffff}, {0x06, 0, 0, 0000000000},
};

struct sock_fprog bpf = {
    .len = 6,
    .filter = f,
};
#define alignup(p, align) (((p) + (align)-1) & (-(align)))
static struct ipt_entry *build_rule(uint16_t port) {
	struct ipt_entry *e;
	size_t size_ipt_entry = alignup(sizeof(struct ipt_entry), 8);
	size_t target_offset =
	    size_ipt_entry + alignup(sizeof(struct xt_entry_match) + sizeof(struct xt_tcp), 8);
	size_t total_size = target_offset + alignup(sizeof(struct xt_standard_target), 8);

	int ret = posix_memalign((void *)&e, 8, total_size);
	assert(!ret);

	memset(e, 0, total_size);
	e->target_offset = target_offset;
	e->next_offset = total_size;

	e->ip.proto = IPPROTO_TCP;
	struct ipt_entry_match *m = (void *)e->elems;
	m->u.user.match_size = target_offset - size_ipt_entry;
	strncpy(m->u.user.name, "tcp", 4);

	struct xt_tcp *tcp = (void *)m->data;
	tcp->dpts[0] = tcp->dpts[1] = port;
	tcp->spts[0] = 0;
	tcp->spts[1] = 0xffff;
	tcp->invflags = 0;
	tcp->option = 0;
	tcp->flg_mask = 0;

	struct xt_entry_target *tgt = (void *)e + target_offset;
	tgt->u.user.target_size = total_size - target_offset;
	strncpy(tgt->u.user.name, "DROP", 5);
	return e;
}

int init_ftcp(struct faketcp *s, uint32_t saddr, uint16_t port) {
	struct ipt_entry *e = NULL;
	struct xtc_handle *h = NULL;
	memset(s, 0, sizeof *s);
	s->rcvfd = s->sndfd = -1;
	s->rcvfd = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_IP));
	if (s->rcvfd < 0) {
		perror("packet socket");
		goto cleanup;
	}
	f[3].k = port;        // matching port
	int ret = setsockopt(s->rcvfd, SOL_SOCKET, SO_ATTACH_FILTER, &bpf, sizeof(bpf));
	if (ret < 0) {
		perror("filter");
		goto cleanup;
	}

	s->sndfd = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
	if (s->sndfd < 0) {
		perror("raw socket");
		goto cleanup;
	}

	struct sockaddr_in a;
	memset(&a, 0, sizeof a);
	a.sin_family = AF_INET;
	a.sin_addr.s_addr = saddr;
	ret = bind(s->sndfd, (void *)&a, sizeof a);
	if (ret < 0) {
		perror("bind");
		goto cleanup;
	}
	s->src = saddr;

	s->rcv_port = port;
	e = build_rule(port);
	fprintf(stderr, "iptc\n");
	h = iptc_init("filter");
	if (!h) {
		perror("iptc_init");
		goto cleanup;
	}

	fprintf(stderr, "iptc insert\n");
	//auto fe = iptc_first_rule("INPUT", h);
	//if (fe)
		ret = iptc_insert_entry("INPUT", e, 0, h);
	//else
	//	ret = iptc_append_entry("INPUT", e, h);

	if (ret == 0) {
		fprintf(stderr, "iptc_insert %s\n", iptc_strerror(errno));
		goto cleanup;
	}

	fprintf(stderr, "iptc commit\n");
	ret = iptc_commit(h);
	if (ret == 0) {
		fprintf(stderr, "%s\n", iptc_strerror(errno));
		goto cleanup;
	}
	iptc_free(h);
	free(e);
	return 0;
cleanup:
	if (s->rcvfd >= 0)
		close(s->rcvfd);
	if (s->sndfd >= 0)
		close(s->sndfd);
	if (e)
		free(e);
	if (h)
		iptc_free(h);
	return -1;
}

int close_ftcp(struct faketcp *s) {
	close(s->sndfd);
	close(s->rcvfd);
	struct xtc_handle *h = iptc_init("filter");
	if (!h)
		return -1;

	const struct ipt_entry *iter;
	int n = 0;
	for (iter = iptc_first_rule("INPUT", h); !iter; iter = iptc_next_rule(iter, h), n++) {
		struct xt_entry_match *m = (void *)iter->elems;
		assert(m->u.user.name);
		if (strcmp(m->u.user.name, "tcp") != 0)
			continue;
		struct xt_tcp *mt = (void *)m->data;
		if (mt->flg_cmp != 0 || mt->flg_mask != 0 || mt->invflags != 0 || mt->option != 0)
			continue;
		if (mt->spts[0] != 0 || mt->spts[1] != 0xffff)
			continue;
		if (mt->dpts[0] != mt->dpts[1] || mt->dpts[0] != s->rcv_port)
			continue;
		break;
	}

	int retval = -1;
	int ret = iptc_delete_num_entry("INPUT", n, h);
	if (ret == 0) {
		fprintf(stderr, "delete %d %s\n", n, iptc_strerror(errno));
		goto out;
	}
	ret = iptc_commit(h);
	if (ret == 0)
		goto out;
	retval = 0;
out:
	iptc_free(h);
	return retval;
}

void ftcp_assemble_opt(const struct ftcp_hdr *hdr, uint8_t *buf, size_t *len) {
	uint8_t *pos = buf;
	for (int i = 0; i < hdr->nopt; i++) {
		*(pos++) = hdr->opt[i]->kind;
		*(pos++) = hdr->opt[i]->len;
		memcpy(pos, hdr->opt[i]->val, hdr->opt[i]->len - 2);
		pos += hdr->opt[i]->len - 2;
	}
	int padding = (4 - (pos - buf) % 4) % 4;
	memset(pos, 1, padding);
	*len = (pos - buf) + padding;
}

// data has to be padded to even number of bytes
ssize_t ftcp_snd(struct faketcp *s, const uint8_t *data, size_t len, const struct ftcp_hdr *hdr) {
#define ckl(a) (uint16_t)((a) >> 16) + (uint16_t)((a)&0xffff)
#define bu16(a)                                                                                         \
	memcpy(pos, &((uint16_t){a}), 2);                                                               \
	pos += 2
#define bu32(a)                                                                                         \
	memcpy(pos, &((uint32_t){a}), 4);                                                               \
	pos += 4
	static char buf[IP_MAXPACKET];
	char *pos = buf;
	uint32_t cksum = 0;

	for (size_t i = 0; i < len; i += 2)
		cksum += data[i] << 8;
	for (size_t i = 1; i < len; i += 2)
		cksum += data[i];

	uint8_t optbuf[40];
	size_t optlen;
	ftcp_assemble_opt(hdr, optbuf, &optlen);
	for (size_t i = 0; i < optlen; i += 2)
		cksum += optbuf[i] << 8;
	for (size_t i = 1; i < optlen; i += 2)
		cksum += optbuf[i];

	uint16_t hw12 = ((5 + optlen / 4) << 12) |
	                (hdr->flags & 0x1ff);        // data offset and flags, 16bit at offset 12
	cksum += ckl(ntohl(s->src));
	cksum += ckl(ntohl(hdr->addr));
	cksum += 6;        // TCP protocol
	cksum += len /* data len */ + 20 /* tcp header len */ + optlen;
	cksum += ntohs(hdr->dport);
	cksum += ntohs(hdr->sport);
	cksum += ckl(hdr->seq);
	cksum += ckl(hdr->ack);
	cksum += hw12;
	cksum += 1024;        // window
	cksum = ckl(cksum);

	bu16(hdr->sport);
	bu16(hdr->dport);
	bu32(htonl(hdr->seq));
	bu32(htonl(hdr->ack));
	bu16(htons(hw12));
	bu16(htons(1024));
	bu16(htons(~cksum));
	bu16(0);
	if (optlen) {
		memcpy(pos, optbuf, optlen);
		pos += optlen;
	}
	if (data)
		memcpy(pos, data, len);

	// actually send the packet
	struct sockaddr_in sin;
	memset(&sin, 0, sizeof sin);
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = hdr->addr;

	if (len + optlen + 34 > 1490) {
		fprintf(stderr, "%lu risky\n", len + 34);
	}
	ssize_t ret = sendto(s->sndfd, buf, 20 /*tcp hdr*/ + optlen + len, 0, (void *)&sin, sizeof(sin));
	if (ret < 0)
		perror("sendto");
	return ret;
#undef ckl
#undef bu16
#undef bu32
}

void ftcp_parse_opt(struct ftcp_hdr *hdr) {
	uint8_t *pos = hdr->optbuf;
	int cnt = 0;
	while (1) {
		hdr->opt[cnt++] = (void *)pos;
		if (hdr->opt[cnt - 1]->kind == 0)
			break;
		if (hdr->opt[cnt - 1]->kind == 1)
			pos += 1;
		else
			pos += hdr->opt[cnt - 1]->len;
		if (pos >= hdr->optbuf + hdr->optlen)
			break;
	}
	hdr->nopt = cnt;
}

ssize_t ftcp_rcv(struct faketcp *s, const char **data, struct ftcp_hdr *hdr) {
	static char buf[IP_MAXPACKET];
	struct sockaddr_ll lla;
	socklen_t al = sizeof lla;
	int ret = recvfrom(s->rcvfd, buf, sizeof buf, 0, (void *)&lla, &al);
	if (ret < 0)
		return ret;

	// unpack the hdr
	uint16_t iplen;
	uint8_t ihl = ((*buf) & 0xf) * 4;
	memcpy(&iplen, buf + 2, 2);
	iplen = ntohs(iplen);
	uint32_t src, dst;
	memcpy(&src, buf + 12, sizeof src);
	memcpy(&dst, buf + 16, sizeof dst);
	if (dst != s->src)
		// not for us (put this into the filter?)
		return 0;
	hdr->addr = src;
	memcpy(&hdr->sport, buf + 20, 2);
	memcpy(&hdr->dport, buf + 22, 2);
	uint16_t hw12;
	uint32_t tmp;
	memcpy(&tmp, buf + 24, 4);
	hdr->seq = ntohl(tmp);
	memcpy(&tmp, buf + 28, 4);
	hdr->ack = ntohl(tmp);
	memcpy(&hw12, buf + 32, 2);
	hw12 = ntohs(hw12);
	hdr->flags = hw12 & 0x1ff;
	hdr->nopt = 0;
	int offset = ((hw12 >> 12) & 0xf) * 4;
	// printf("off: %d %d %d\n", offset, ret, iplen);
	if (ret < ihl + offset) {
		// malformed packet
		*data = NULL;
		return -1;
	}
	if (offset > 20) {
		hdr->optlen = offset - 20;
		memcpy(hdr->optbuf, buf + ihl + 20, offset - 20);
		ftcp_parse_opt(hdr);
	}
	size_t len = 0;
	if (ret > 20 + offset) {
		*data = buf + 20 + offset;
		len = iplen - 20 - offset;
	} else
		*data = NULL;

	return len;
}
