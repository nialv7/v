#include <arpa/inet.h>
#include <assert.h>
#include <ev.h>
#include <netinet/ip.h>
#include <poll.h>
#include <sodium/crypto_aead_xchacha20poly1305.h>
#include <sodium/crypto_generichash.h>
#include <sodium/crypto_kx.h>
#include <sodium/randombytes.h>
#include <sodium/utils.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "common.h"
#include "ftcp.h"
#include "tun.h"
#include "vcom.h"

#define auto __auto_type
#define TTL 120
#define REPLAY_WIN 256

_Static_assert(crypto_pwhash_SALTBYTES <= crypto_kx_SESSIONKEYBYTES, "kx session key is too small");

static struct connection *current = NULL;
struct pending {
	struct connection *c;
	uint8_t challenge[32];
	uint8_t secret[crypto_kx_SESSIONKEYBYTES];
	struct pending *next, **prev;
};

enum state {
	NO_CONN,
	SYN_RCV,        // not used. if peer is found in pending, then it's in SYN_RCV state
	CONN,
	COOKIE_SENT,
	COOKIE_SYN_RCV,
};

static struct pending *head;
static int pcnt = 0;
static const int pmax = 16;
static ev_io tunw, sockw;
static ev_timer rst_timer, pulse_timer, cookie_timer;
static struct faketcp s;
static uint16_t sport;
static const char *passwd;
static enum state state;

static void free_pending(struct pending *pp) {
	if (pp->next)
		pp->next->prev = pp->prev;
	*pp->prev = pp->next;
	sodium_memzero(pp->secret, sizeof pp->secret);
	if (pp->c)
		free_connection(pp->c);
	free(pp);
}

static struct pending *find_pending(const struct ftcp_hdr *hdr) {
	struct pending *pp = head;
	while (pp) {
		if (peer_match(&pp->c->p, hdr))
			break;
		pp = pp->next;
	}
	return pp;
}

static ev_tstamp last_rst_cookie;
static inline void rst_cookie(struct faketcp *s, const struct ftcp_hdr *hdr, uint16_t port) {
	fprintf(stderr, "sending RST cookie\n");

	if (state == COOKIE_SYN_RCV)
		fprintf(stderr, "no data transmitted since last RST\n");
	static uint8_t buf[16];
	uint8_t out[64];
	unsigned long long olen;
	randombytes_buf(buf, 16);

	uint8_t nonce[24];
	randombytes_buf(nonce, 24);
	int ret2 = crypto_aead_xchacha20poly1305_ietf_encrypt(out + 24, &olen, buf, 16, NULL, 0, NULL,
	                                                      nonce, current->snd_key);
	if (ret2 != 0) {
		rst(s, hdr);
		return;
	}
	memcpy(out, nonce, 24);
	rst_with_data(s, hdr, out, olen + 24);
	sodium_memzero(out, 64);

	current->cookie = buf;
	state = COOKIE_SENT;
	ev_timer_stop(EV_DEFAULT, &rst_timer);
}

static void conn(void) {
	assert(current);
	state = CONN;
	ev_io_start(EV_DEFAULT, &tunw);
	pulse_timer.repeat = random();
	pulse_timer.repeat /= RAND_MAX / 2;
	ev_timer_again(EV_DEFAULT, &pulse_timer);
	rst_timer.repeat = random();
	rst_timer.repeat /= RAND_MAX / 120;
	ev_timer_again(EV_DEFAULT, &rst_timer);
}
static void no_conn(void) {
	fprintf(stderr, "no conn\n");
	state = NO_CONN;
	if (current)
		free_connection(current);
	current = NULL;
	ev_io_stop(EV_DEFAULT, &tunw);
	ev_timer_stop(EV_DEFAULT, &pulse_timer);
	ev_timer_stop(EV_DEFAULT, &rst_timer);
}

static inline void client_auth(struct faketcp *s, struct ftcp_hdr *hdr, uint16_t port,
                               const uint8_t *data, size_t dlen, const char *passwd) {
	if (dlen != crypto_onetimeauth_BYTES) {
		fprintf(stderr, "ACK wrong length %s %d\n", inet_ntoa((struct in_addr){hdr->addr}),
		        ntohs(hdr->sport));
		rst(s, hdr);
		return;
	}
	// connection hasn't been established
	// this is the client auth packet
	// format:
	// auth
	auto pp = find_pending(hdr);
	if (!pp) {
		fprintf(stderr, "spurious ACK %s %d\n", inet_ntoa((struct in_addr){hdr->addr}),
		        ntohs(hdr->sport));
		rst(s, hdr);
		return;
	}
	fprintf(stderr, "new connection ACK\n");
	if (verify_challenge(pp->challenge, 32, pp->secret, passwd, data) != 0) {
		fprintf(stderr, "failed to verify client ACK\n");
		// not sending RST
		goto removepp;
	}
	if (current)
		free_connection(current);
	pp->c->tseq = pp->c->rseq = 0;
	current = pp->c;
	pp->c = NULL;
	conn();
removepp:
	free_pending(pp);
	pcnt--;
}

static inline void new_pending_connection(struct faketcp *s, const struct ftcp_hdr *hdr, uint16_t port,
                                          const uint8_t *data, size_t dlen, const char *passwd) {
	uint8_t buf[IP_MAXPACKET];
	fprintf(stderr, "new connection SYN\n");
	uint8_t pk[3][crypto_kx_PUBLICKEYBYTES];
	uint8_t sk[3][crypto_kx_SECRETKEYBYTES];
	uint8_t tmp[2][crypto_kx_SESSIONKEYBYTES];
	uint8_t auth[crypto_onetimeauth_BYTES];
	// format
	// pk1 (sess key) || pk2 (auth secret) || challenge
	if (dlen != 2 * crypto_kx_PUBLICKEYBYTES + 32) {
		rst(s, hdr);
		return;
	}
	if (current && peer_match(&current->p, hdr)) {
		fprintf(stderr, "re-syn for client\n");
		free_connection(current);
		current = NULL;
	}

	for (int i = 0; i < 2; i++)
		crypto_kx_keypair(pk[i], sk[i]);
	const uint8_t *cpk[3];
	cpk[0] = (void *)data;
	cpk[1] = (void *)data + crypto_kx_PUBLICKEYBYTES;

	struct connection *c;
	auto pp = find_pending(hdr);
	if (pp)
		// we have a record, so reuse it
		c = pp->c;
	else
		c = malloc(sizeof *c);
	c->cookie = NULL;
	int ret2 = crypto_kx_server_session_keys(c->rcv_key, c->snd_key, pk[0], sk[0], cpk[0]);
	sodium_memzero(sk[0], sizeof sk[0]);
	if (ret2 != 0) {
		free(c);
		return;
	}
	ret2 = crypto_kx_server_session_keys(tmp[0], tmp[1], pk[1], sk[1], cpk[1]);
	if (ret2 != 0) {
		free(c);
		return;
	}

	const uint8_t *ch = data + 2 * crypto_kx_PUBLICKEYBYTES;
	compute_challenge(ch, 32, tmp[0], passwd, auth);
	c->p.src = hdr->addr;
	c->p.sport = hdr->sport;

	if (pcnt >= pmax)
		free_pending(head);
	else
		pcnt++;

	if (!pp) {
		pp = malloc(sizeof *pp);
		pp->c = c;
		pp->prev = &head;
		pp->next = head;
		head = pp;
	}
	randombytes_buf(pp->challenge, 32);
	memcpy(pp->secret, tmp[1], crypto_kx_SESSIONKEYBYTES);

	// reply format
	// spk1 || spk2 || challenge_resp || challenge2
	uint8_t *pos = buf;
	memcpy(pos, pk[0], sizeof pk[0]);
	pos += sizeof pk[0];
	memcpy(pos, pk[1], sizeof pk[1]);
	pos += sizeof pk[1];
	memcpy(pos, auth, sizeof auth);
	pos += sizeof auth;
	memcpy(pos, pp->challenge, 32);
	pos += 32;

	struct ftcp_hdr nhdr = *hdr;
	nhdr.dport = hdr->sport;
	nhdr.sport = htons(port);
	nhdr.flags = 2 | 16;        // SYN ACK
	nhdr.ack = hdr->seq + dlen + 1;
	nhdr.seq = randombytes_random();
	nhdr.nopt = 0;
	ftcp_snd(s, buf, pos - buf, &nhdr);
}

static inline int restablish_connection(struct faketcp *s, struct ftcp_hdr *hdr, uint16_t port,
                                        const uint8_t *data, size_t dlen) {
	int i;
	for (i = 0; i < hdr->nopt; i++) {
		if (hdr->opt[i]->kind == 34)
			goto found;
	}
	// hold onto current connection even if
	// we recive invalid syncookies

	// connection switch only happens after
	// a successful re-auth
	return -1;
found:
	if (state != COOKIE_SENT && state != COOKIE_SYN_RCV) {
		fprintf(stderr, "unexpected cookie\n");
		rst(s, hdr);
		return 0;
	}
	// verify cookie
	assert(current->cookie);
	if (sodium_memcmp(hdr->opt[i]->val, current->cookie, 16) != 0) {
		fprintf(stderr, "got invalid cookie\n");
		if (state == COOKIE_SYN_RCV)
			// already got a valid SYN
			rst(s, hdr);
		return 0;
	}

	state = COOKIE_SYN_RCV;
	struct ftcp_hdr nhdr;
	nhdr.addr = hdr->addr;
	nhdr.dport = hdr->sport;
	nhdr.sport = htons(port);
	nhdr.flags = 2 | 16;        // SYN ACK
	nhdr.ack = hdr->seq + 1;
	nhdr.seq = randombytes_random();
	nhdr.nopt = 0;

	current->ack = hdr->seq + 1;
	current->seq = nhdr.seq + 1;

	// remote float
	fprintf(stderr, "got syn cookie\n");
	dump_hdr(hdr);
	struct pending *pp = find_pending(hdr);
	if (pp)
		free_pending(pp);
	current->p.src = hdr->addr;
	current->p.sport = hdr->sport;
	ftcp_snd(s, NULL, 0, &nhdr);
	ev_timer_again(EV_DEFAULT, &rst_timer);
	return 0;
}
static void sock_cb(EV_P_ ev_io *w, int revents) {
	const char *rep = NULL;
	struct ftcp_hdr hdr;
	ssize_t ret = ftcp_rcv(&s, &rep, &hdr);
	if (ret < 0)
		return;
	if (hdr.flags == 2) {        // SYN
		if (restablish_connection(&s, &hdr, sport, (const uint8_t *)rep, ret) != 0)
			new_pending_connection(&s, &hdr, sport, (const uint8_t *)rep, ret, passwd);
	} else if (hdr.flags == 4) {        // RST
		// remove from current and pending
		// connections
		if (state == CONN && peer_match(&current->p, &hdr)) {
			fprintf(stderr, "received RST for "
			                "current\n");
			no_conn();
		} else {
			auto pp = find_pending(&hdr);
			if (pp)
				free_pending(pp);
			pcnt--;
		}
	} else if (hdr.flags == 16) {
		if (state == NO_CONN || !peer_match(&current->p, &hdr))
			client_auth(&s, &hdr, sport, (const uint8_t *)rep, ret, passwd);
		else {
			switch (state) {
			case CONN: remote2tun(tunw.fd, &hdr, (const uint8_t *)rep, ret, current); break;
			case COOKIE_SENT:
				// the rst cookie might be lost
				// rst
				//no_conn();
				//rst(&s, &hdr);
				break;
			case COOKIE_SYN_RCV:
				if (ret != 0)
					remote2tun(tunw.fd, &hdr, cu8(rep), ret, current);
				fprintf(stderr, "got reconnect ack\n");
				sodium_memzero(current->cookie, 16);
				current->cookie = NULL;
				conn();
				break;
			default:
				assert(0);
			}
		}
	}
}
static size_t queued;
static uint8_t data_queue[IP_MAXPACKET];
static void tun_timeout_cb(EV_P_ ev_timer *t, int revents) {
	ev_timer_stop(EV_A, &tun_timer);
	if (state != CONN)
		return;
	if (queued < PWATERMARK) {
		queued += addpad(data_queue+queued, PWATERMARK-queued);
	}
	buf2remote(data_queue, queued, sport, &s, current);
	queued = 0;
}
static void tun_cb(EV_P_ ev_io *w, int revents) {
	if (state != CONN)
		return;
	tun2buf(w->fd, data_queue, &queued, sport, &s, current);
	ev_timer_again(EV_DEFAULT, &pulse_timer);
}

static void rst_timeout(EV_P_ ev_timer *t, int revents) {
	if (!current) {
		ev_timer_stop(EV_A_ t);
		return;
	}
	struct ftcp_hdr hdr;
	hdr.addr = current->p.src;
	hdr.sport = current->p.sport;
	hdr.seq = current->ack;
	hdr.ack = current->seq;

	ev_io_stop(EV_DEFAULT, &tunw);
	ev_timer_stop(EV_DEFAULT, &pulse_timer);
	rst_cookie(&s, &hdr, sport);
}

static void pulse_timeout(EV_P_ ev_timer *t, int revents) {
	struct ftcp_hdr hdr;
	hdr.addr = current->p.src;
	hdr.sport = htons(s.rcv_port);
	hdr.dport = current->p.sport;
	hdr.seq = current->seq;
	hdr.ack = current->ack;
	hdr.nopt = 0;
	hdr.flags = 16; // ACK;

	char buf[4096];
	int len = random()%PWATERMARK + 1;
	randombytes_buf(buf, len);
	fprintf(stderr, "sending pulse\n");
	ftcp_snd(&s, buf, len, &hdr);
	pulse_timer.repeat = random();
	pulse_timer.repeat /= RAND_MAX / 2;
	ev_timer_again(EV_DEFAULT, &pulse_timer);
}

int main(int argc, const char **argv) {
	if (argc < 5)
		return -1;
	ev_init(&tun_timer, tun_timeout_cb);
	tun_timer.repeat = 0.05;
	ev_init(&rst_timer, rst_timeout);
	rst_timer.repeat = 80;
	ev_init(&pulse_timer, pulse_timeout);
	pulse_timer.repeat = 1;
	struct ftcp_hdr hdr;
	uint32_t saddr;
	sport = atoi(argv[2]);
	inet_aton(argv[1], (void *)&saddr);
	ssize_t ret = init_ftcp(&s, saddr, sport);
	assert(ret >= 0);
	passwd = argv[4];

	state = NO_CONN;

	int tfd = create_tuntap(argv[3], 1);

	ev_io_init(&tunw, tun_cb, tfd, EV_READ);
	ev_io_init(&sockw, sock_cb, s.rcvfd, EV_READ);
	ev_io_start(EV_DEFAULT, &sockw);

	ev_run(EV_DEFAULT, 0);
	return 0;
}
