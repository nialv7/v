#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include "ftcp.h"
#include "common.h"
int main(int argc, const char **argv) {
	if (argc < 3)
		return 1;
	struct faketcp s;
	uint32_t saddr;
	inet_aton(argv[1], (void *)&saddr);
	init_ftcp(&s, saddr, 1999);
	struct ftcp_hdr hdr;
	inet_aton(argv[2], (void *)&hdr.addr);
	seed_rand();
	hdr.seq = rand();
	hdr.sport = htons(23987);
	hdr.dport = htons(18927);
	hdr.flags = 4; // SYN
	char data[] = "ab";
	ftcp_snd(&s, (void *)data, 3, &hdr);

	const char *rep = NULL;
	ssize_t ret = ftcp_rcv(&s, &rep, &hdr);
	dump_hdr(&hdr);
	if (ret)
		printf("%s\n", rep);

	hdr.sport = htons(1999);
	hdr.dport = htons(2000);
	hdr.flags = 16;// ACK
	char data2[] = "fg";
	ftcp_snd(&s, (void *)data2, 3, &hdr);
	char buf[20];
	while(fgets(buf, sizeof buf, stdin) != NULL) {
		hdr.sport = htons(1999);
		hdr.dport = htons(2000);
		hdr.flags = 16; //ACK
		ftcp_snd(&s, (void *)buf, strlen(buf)+1, &hdr);

		int ret = ftcp_rcv(&s, &rep, &hdr);
		dump_hdr(&hdr);
		if (ret)
			printf("%s\n", rep);
	}
	hdr.sport = htons(1999);
	hdr.dport = htons(2000);
	hdr.flags = 4;// RST
	ftcp_snd(&s, (void *)data2, 0, &hdr);
}
