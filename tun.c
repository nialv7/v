#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <linux/if_ether.h>
#include <linux/if_tun.h>
#include <net/if.h>
#include <sys/ioctl.h>
int create_tuntap(const char *name, int tunmode) {
	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	int ret = open("/dev/net/tun", O_RDWR);
	if (ret < 0) {
		perror("open");
		return -1;
	}
	strcpy(ifr.ifr_name, name);
	ifr.ifr_flags = tunmode ? IFF_TUN : IFF_TAP;
	ifr.ifr_flags |= IFF_NO_PI;
	if (ioctl(ret, TUNSETIFF, (void *)&ifr) < 0) {
		perror("ioctl");
		return -1;
	}
	return ret;
}
