#pragma once

#include <assert.h>
#include <ev.h>
#include <netinet/ip.h>
#include <sodium/crypto_aead_xchacha20poly1305.h>
#include <sodium/crypto_kx.h>
#include <sodium/crypto_onetimeauth.h>
#include <sodium/crypto_pwhash.h>
#include <sodium/randombytes.h>
#include <sodium/utils.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "ftcp.h"
struct peer {
	uint32_t src;
	uint16_t sport;
};
struct connection {
	struct peer p;
	uint8_t snd_key[crypto_aead_xchacha20poly1305_ietf_KEYBYTES];
	uint8_t rcv_key[crypto_aead_xchacha20poly1305_ietf_KEYBYTES];
	uint32_t seq, ack;
	uint64_t tseq, rseq;
	uint8_t *cookie;
};
// out is crypto_onetimeauth_BYTES
// secret is at least crypto_pwhash_SALTBYTES
static inline int compute_challenge(const uint8_t *challenge, size_t clen, const uint8_t *secret,
                                    const char *passwd, uint8_t *out) {
	static uint8_t key[crypto_onetimeauth_KEYBYTES];
	int ret = crypto_pwhash(key, sizeof key, passwd, strlen(passwd), secret, 3, 8 * 1024 * 1024,
	                        crypto_pwhash_ALG_DEFAULT);
	assert(!ret);
	ret = crypto_onetimeauth(out, challenge, clen, key);
	assert(!ret);
	return 0;
}
static inline bool verify_challenge(const uint8_t *challenge, size_t clen, const uint8_t *secret,
                                    const char *passwd, const uint8_t *auth) {
	static uint8_t key[crypto_onetimeauth_KEYBYTES];
	int ret = crypto_pwhash(key, sizeof key, passwd, strlen(passwd), secret, 3, 8 * 1024 * 1024,
	                        crypto_pwhash_ALG_DEFAULT);
	assert(!ret);
	return crypto_onetimeauth_verify(auth, challenge, clen, key);
}
static inline bool peer_match(struct peer *p, const struct ftcp_hdr *h) {
	return p->src == h->addr && p->sport == h->sport;
}
static inline void free_connection(struct connection *c) {
	sodium_memzero(c->snd_key, sizeof c->snd_key);
	sodium_memzero(c->rcv_key, sizeof c->rcv_key);
	free(c);
}

static inline void buf2remote(const uint8_t *buf, size_t inlen, uint16_t port, struct faketcp *s,
                              struct connection *current) {
	if (!current)
		// connection not established yet
		// drop packet
		return;
	if (!inlen)
		return;
	uint8_t out[IP_MAXPACKET];
	unsigned long long olen = (sizeof out) - 8;

	current->tseq++;
	uint8_t nonce[24];
	randombytes_buf(nonce, 24);
	int ret2 = crypto_aead_xchacha20poly1305_ietf_encrypt(out + 26, &olen, buf, inlen, NULL, 0, NULL,
	                                                      nonce, current->snd_key);
	uint16_t plen = olen + 26;
	memcpy(out, &((uint16_t){htons(plen)}), 2);
	memcpy(out + 2, nonce, 24);
	if (ret2 != 0)
		return;

	struct ftcp_hdr hdr;
	hdr.addr = current->p.src;
	hdr.sport = htons(port);
	hdr.dport = current->p.sport;
	hdr.flags = 16;        // ACK
	hdr.ack = current->ack;
	hdr.seq = current->seq;
	hdr.nopt = 0;
	current->seq += plen;
	ftcp_snd(s, out, plen, &hdr);
}

#define PWATERMARK 1410

static ev_timer tun_timer;
static inline size_t addpad(uint8_t *out, size_t max) {
	if (max <= 3) {
		return 0;
	}
	uint16_t len = random()%(max-2);
	memcpy(out, (uint8_t[]){0}, 1);
	memcpy(out+1, (uint16_t[]){htons(len)}, 2);
	randombytes_buf(out+3, len);
	return len+3;
}
static inline void tun2buf(int tfd, uint8_t *out, size_t *pos, uint16_t port, struct faketcp *s,
                           struct connection *current) {
	// tun incoming
	char tmpbuf[IP_MAXPACKET];
	size_t cp = *pos;
	ssize_t ret = read(tfd, tmpbuf, IP_MAXPACKET);
	if (ret <= 0)
		return;
	if (cp + ret + 2 > PWATERMARK) {
		buf2remote(out, cp, port, s, current);
		cp = 0;
	}
	if (ret > PWATERMARK) {
		fprintf(stderr, "Message tooo long\n");
		return;
	}
	if (cp == 0)
		ev_timer_again(EV_DEFAULT, &tun_timer);
	// real packet marker
	memcpy(out + cp, (uint8_t[]){1}, 1);
	memcpy(out + cp + 1, (uint16_t[]){htons(ret)}, 2);
	memcpy(out + cp + 3, tmpbuf, ret);
	*pos = cp + ret + 3;
}

// XXX: fix replay
#define REPLAY_WIN 256
static inline void remote2tun(int tfd, const struct ftcp_hdr *hdr, const uint8_t *data, size_t len,
                              struct connection *current) {
	uint8_t buf[IP_MAXPACKET];
	uint8_t nonce[24];
	uint16_t plen;
	const uint8_t *pos = data;
	if (!current)
		return;
	if (!data)
		return;
#if 0
	if (rseq < current->rseq && rseq + REPLAY_WIN < current->rseq)
		// rudimentary replay attack
		// prevention
		return;
#endif
next:
	if (pos + 26 > data + len)
		return;
	memcpy(&plen, pos, 2);
	memcpy(nonce, pos + 2, 24);
	plen = ntohs(plen);

	unsigned long long mlen = sizeof buf;
	int ret2 = crypto_aead_xchacha20poly1305_ietf_decrypt(buf, &mlen, NULL, pos + 26, plen - 26,
	                                                      NULL, 0, nonce, current->rcv_key);
	if (ret2 != 0) {
		fprintf(stderr, "decryption failure %lu\n", len);
		return;
	}
	if (hdr->seq + len > current->ack)
		current->ack = hdr->seq + len;

	const uint8_t *pos2 = buf;
	while (pos2 < buf + mlen) {
		uint16_t slen;
		memcpy(&slen, pos2+1, 2);
		uint8_t real = *(uint8_t *)pos2;
		slen = ntohs(slen);
		if (real) {
			ret2 = write(tfd, pos2 + 3, (size_t)slen);
			if (ret2 < 0)
				perror("tun write");
		}
		pos2 += 3 + slen;
	}
	pos += plen;
	goto next;
}

static inline void
rst_with_data(struct faketcp *s, const struct ftcp_hdr *hdr, const uint8_t *data, size_t dlen) {
	struct ftcp_hdr nhdr = *hdr;
	nhdr.dport = hdr->sport;
	nhdr.sport = htons(s->rcv_port);
	nhdr.flags = 4 | 16;        // RST ACK
	nhdr.ack = hdr->seq;
	nhdr.seq = hdr->ack;
	nhdr.nopt = 0;
	ftcp_snd(s, data, dlen, &nhdr);
}

#define rst(s, hdr) rst_with_data(s, hdr, NULL, 0)
#define cu8(x) ((const uint8_t *)(x))
