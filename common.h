#pragma once
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include "ftcp.h"


static inline void seed_rand(void) {
	int fd = open("/dev/urandom", O_RDONLY);
	uint32_t buf;
	read(fd, &buf, sizeof buf);
	close(fd);
	srand(buf);
}
