#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdio.h>
#include "ftcp.h"
#include "common.h"
int main(int argc, const char **argv) {
	if (argc < 2)
		return -1;
	struct faketcp s;
	uint32_t saddr;
	inet_aton(argv[1], (void *)&saddr);
	init_ftcp(&s, saddr, 2000);
	struct ftcp_hdr hdr;

	const char *rep = NULL;
	ssize_t ret = ftcp_rcv(&s, &rep, &hdr);
	dump_hdr(&hdr);
	if (ret)
		printf("%s\n", rep);

	char data[] = "cd";
	seed_rand();
	s.seq = rand();
	hdr.dport = hdr.sport;
	hdr.sport = htons(2000);
	hdr.flags = 2|16; //SYN ACK
	ftcp_snd(&s, (void *)data, 3, &hdr);

	s.ack = 0;
	s.seq = 1;
	ret = ftcp_rcv(&s, &rep, &hdr);
	dump_hdr(&hdr);
	if (ret)
		printf("%s\n", rep);
	while(1) {
		int ret = ftcp_rcv(&s, &rep, &hdr);
		dump_hdr(&hdr);
		if (ret)
			printf("%s\n", rep);
		if (hdr.flags&4)
			break;
		char pong[] = "pong";
		s.seq=10;
		s.ack=3;
		hdr.dport = hdr.sport;
		hdr.sport = htons(2000);
		hdr.flags = 16;
		ftcp_snd(&s, (void *)pong, 5, &hdr);
	}
}
